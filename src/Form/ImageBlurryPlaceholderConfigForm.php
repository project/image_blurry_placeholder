<?php

namespace Drupal\image_blurry_placeholder\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for the Image Blurry Placeholder module.
 */
class ImageBlurryPlaceholderConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'image_blurry_placeholder.settings';

  /**
   * Implements getEditableConfigNames().
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Implements getFormId().
   */
  public function getFormId() {
    return 'image_blurry_placeholder_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['use_css_js_blur'] = [
      '#title' => $this->t('Use Javascript to apply/remove the "loaded" class on images. Add CSS blur on images not yet loaded.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('use_css_js_blur'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $use_css_js_blur = $form_state->getValue('use_css_js_blur');

    $this->config(static::SETTINGS)
      ->set('use_css_js_blur', $use_css_js_blur)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
