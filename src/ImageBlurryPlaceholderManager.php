<?php

namespace Drupal\image_blurry_placeholder;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\image_blurry_placeholder\Form\ImageBlurryPlaceholderConfigForm;

/**
 * Contains helpful functions for the module.
 */
class ImageBlurryPlaceholderManager {

  /**
   * A list of supported image plugins.
   */
  const SUPPORTED_IMAGE_PLUGINS = [
    'media_responsive_thumbnail',
    'media_thumbnail',
    'image',
    'responsive_image',
  ];

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a ImageBlurryPlaceholderManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get(ImageBlurryPlaceholderConfigForm::SETTINGS);
  }

  /**
   * Check if a formatter plugin ID is supported for blurry placeholder.
   *
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return bool
   *   True if the plugin is supported.
   */
  public function isSupportedPlugin(string $plugin_id) {
    return in_array($plugin_id, self::SUPPORTED_IMAGE_PLUGINS);
  }

  /**
   * Test if a field should be processed by this module.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   * @param string $view_mode
   *   The view mode.
   * @param string $field_name
   *   The field name.
   *
   * @return bool
   *   True if the field should be processed.
   */
  public function shouldProcessField(FieldableEntityInterface $entity, string $view_mode, string $field_name) {
    // Get the field formatter settings.
    $entity_display = EntityViewDisplay::collectRenderDisplay($entity, $view_mode);
    $field_display = $entity_display->getComponent($field_name);

    if (!$this->isSupportedPlugin($field_display['type'])) {
      return FALSE;
    }

    if (empty($field_display['third_party_settings']['image_blurry_placeholder']['use_blurry_placeholder'])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Check if the Javascript library should be loaded.
   *
   * @return bool
   *   True if the Javascript library should be loaded.
   */
  public function shouldLoadJavascriptLibrary() {
    return !empty($this->config->get('use_css_js_blur'));
  }

}
