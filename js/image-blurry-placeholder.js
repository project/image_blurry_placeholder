(function () {
  Drupal.behaviors.imageBluryPlaceholder = {
    attach: function (context, settings) {
      // Select all <img> elements on the page.
      const images = document.querySelectorAll('img.img-blurry-placeholder');

      // Define a function to add the class after the image has loaded.
      function addClassOnLoad(img) {
        img.classList.remove('loading');
      }

      // Loop through all <img> elements and add an event listener to each one.
      images.forEach(img => {
        if (!img.complete) {
          img.classList.add('loading');
          img.addEventListener('load', () => {
            addClassOnLoad(img);
          });
        }
      });
    }
  };
})();
